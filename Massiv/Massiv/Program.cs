﻿using System;
using System.Linq;

class Program
{
    static void Main()
    {
        //task 0
        int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        Console.WriteLine("Please enter the number");
        int searchNumber;
        if (int.TryParse(Console.ReadLine(), out searchNumber))
        {
            if (numbers.Contains(searchNumber))
            {
                Console.WriteLine($"{searchNumber} is in array.");
            }
            else
            {
                Console.WriteLine($"{searchNumber} is not in array.");
            }
        }
        else
        {
            Console.WriteLine("Please check your input and try again.");
        }


        //Task 1
        int[] numbers1 = { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };

        Console.WriteLine("Please enter the number that needs to be deleted");
        int numberToRemove;
        if (int.TryParse(Console.ReadLine(), out numberToRemove))
        {
            int[] updatedNumbers = numbers1.Where(n => n != numberToRemove).ToArray();

            if (updatedNumbers.Length != numbers1.Length)
            {
                Console.WriteLine($"{numberToRemove} was deleted.");
            }
            else
            {
                Console.WriteLine($"{numberToRemove} not found in array.");
            }
            Console.WriteLine("Updated array");
            foreach (int number in updatedNumbers)
            {
                Console.WriteLine($"{number} ");
            }
        }
        else
        {
            Console.WriteLine("Please check your input and try again.");
        }


        //Task 2
        Console.WriteLine("Enter the size of array");
        int arraySize;
        if (int.TryParse(Console.ReadLine(), out arraySize) && arraySize > 0)
        {
            Random random = new Random();

            int[] numbers2 = new int[arraySize];
            for (int i = 0; i < arraySize; i++)
            {
                numbers2[i] = random.Next(1, 101);
            }
            int max = numbers2[0];
            int min = numbers2[0];
            double sum = numbers2[0];

            for (int i = 1; i < arraySize; i++)
            {
                if (numbers2[i] > max)
                {
                    max = numbers2[i];
                }

                if (numbers2[i] < min)
                {
                    min = numbers2[i];
                }

                sum += numbers2[i];
            }

            double average = sum / arraySize;

            Console.WriteLine($"Max vakue {max}");
            Console.WriteLine($"Min value {min}");
            Console.WriteLine($"Middle value {average}");
        }
        else
        {
            Console.WriteLine("Please check your input and try again.");
        }


        //Task 3
        int[] array1 = { 10, 15, 20, 25, 30 };
            int[] array2 = { 5, 10, 15, 20, 25 };

            Console.WriteLine("Array 1");
            PrintArray(array1);

            Console.WriteLine("\nArray 2");
            PrintArray(array2);

            double average1 = CalculateAverage(array1);
            double average2 = CalculateAverage(array2);

            if (average1 > average2)
            {
                Console.WriteLine($"\nAverage of array 1 is bigger {average1} > {average2}");
            }
            else if (average1 < average2)
            {
                Console.WriteLine($"\nAverage of array 2 is bigger {average2} > {average1}");
            }
            else
            {
                Console.WriteLine("\nAverage for arrays 1 and 2 are equal");
            }
        }

        static void PrintArray(int[] arr)
        {
            foreach (int num in arr)
            {
                Console.Write(num + " ");
            }
        }

        static double CalculateAverage(int[] arr)
        {
            int sum = 0;
            foreach (int num in arr)
            {
                sum += num;
            }

            return (double)sum / arr.Length;
        }
}